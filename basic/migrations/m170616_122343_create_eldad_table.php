<?php

use yii\db\Migration;

/**
 * Handles the creation of table `eldad`.
 */
class m170616_122343_create_eldad_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('eldad', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'email' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('eldad');
    }
}
